
import textwrap
# from unstructured.partition.pdf import partition_pdf
from pathlib import Path
import weaviate
from weaviate.embedded import EmbeddedOptions
import os

import logging


import PyPDF2
logging.basicConfig(level=logging.INFO)

def pdf_to_text(pdf_path):
    with open(pdf_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        num_pages = len(pdf_reader.pages)

        text = ""
        for page_num in range(num_pages):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()

        return text


class AbstractExtractor:
    def __init__(self):
        self.current_section = None  # Keep track of the current section being processed
        self.have_extracted_abstract = (
            False  # Keep track of whether the abstract has been extracted
        )
        self.in_abstract_section = (
            False  # Keep track of whether we're inside the Abstract section
        )
        self.texts = []  # Keep track of the extracted abstract text

    def process(self, element):
        if element.category == "Title":
            self.set_section(element.text)

            if self.current_section == "Abstract":
                self.in_abstract_section = True
                return True

            if self.in_abstract_section:
                return False

        if self.in_abstract_section and element.category == "NarrativeText":
            self.consume_abstract_text(element.text)
            return True

        return True

    def set_section(self, text):
        self.current_section = text
        logging.info(f"Current section: {self.current_section}")

    def consume_abstract_text(self, text):
        logging.info(f"Abstract part extracted: {text}")
        self.texts.append(text)

    def consume_elements(self, elements):
        for element in elements:
            should_continue = self.process(element)

            if not should_continue:
                self.have_extracted_abstract = True
                break

        if not self.have_extracted_abstract:
            logging.warning("No abstract found in the given list of objects.")

    def abstract(self):
        return "\n".join(self.texts)


client = weaviate.Client(
    embedded_options=EmbeddedOptions(
        additional_env_vars={"OPENAI_APIKEY": "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"}
    )
)


client.schema.delete_all()

schema = {
    "class": "Document",
    "vectorizer": "text2vec-openai",
    "properties": [
        {
            "name": "source",
            "dataType": ["text"],
        },
        {
            "name": "abstract",
            "dataType": ["text"],
            "moduleConfig": {
                "text2vec-openai": {"skip": False, "vectorizePropertyName": False}
            },
        },
    ],
    "moduleConfig": {
        "generative-openai": {},
        "text2vec-openai": {"model": "ada", "modelVersion": "002", "type": "text"},
    },
}

client.schema.create_class(schema)


data_folder = "./data"

data_objects = []

for path in Path(data_folder).iterdir():
    if path.suffix != ".pdf":
        continue

    print(f"Processing {path.name}...")

    # elements = partition_pdf(filename=path, strategy="ocr_only")
    
    # abstract_extractor = AbstractExtractor()
    # abstract_extractor.consume_elements(elements)

    # data_object = {"source": path.name, "abstract": abstract_extractor.abstract()}
    data_object = {"source": path.name, "abstract": pdf_to_text("data/" + path.name)}

    data_objects.append(data_object)


with client.batch as batch:
    for data_object in data_objects:
        batch.add_data_object(data_object, "Document")




prompt = """
Please summarize the following academic abstract in a one-liner for a layperson:

{abstract}
"""

results = (
    client.query.get("Document", "source").with_generate(single_prompt=prompt).do()
)

docs = results["data"]["Get"]["Document"]

for doc in docs:
    source = doc["source"]
    abstract = doc["_additional"]["generate"]["singleResult"]
    # wrapped_abstract = textwrap.fill(abstract, width=80)
    print(f"Source: {source}\nSummary:\n{abstract}\n")

