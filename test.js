
const {translate} = require('@vitalets/google-translate-api')
async function f1 (){
  return await translate('Привет, мир! Как дела?', { to: 'en' });
} 
const { text } = f1();

console.log(text) // => 'Hello World! How are you?'
