
from pathlib import Path
from paperqa import Docs

data_folder = "./data"

data_objects = []

for path in Path(data_folder).iterdir():
    if path.suffix != ".pdf":
        continue

    print(f"Processing {path.name}...")
    data_objects.append(data_folder +"/"+  path.name)


docs = Docs(llm='gpt-3.5-turbo')

for d in data_objects:
    docs.add(d)

# print(docs)
answer = docs.query("I am a 25 year old old woman living in Maharashtra. What schemes am I offered?")
print(answer.formatted_answer)



import pickle

# save
with open("my_docs.pkl", "wb") as f:
    pickle.dump(docs, f)

# # load
# with open("my_docs.pkl", "rb") as f:
#     docs = pickle.load(f)
