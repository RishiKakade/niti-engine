from pathlib import Path
from paperqa import Docs
import pickle
from flask import Flask, request

app = Flask(__name__)

# Load the docs from the pickle file
with open("my_docs.pkl", "rb") as f:
    docs = pickle.load(f)

    @app.route('/query', methods=['POST','GET'])
    def query():
        query_text = request.args.get('query')
        result = docs.query(query_text)
        return str(result)


if __name__ == '__main__':
    app.run()

