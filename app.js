const { Client } = require('whatsapp-web.js');
const axios = require('axios');
const { Translate } = require('@google-cloud/translate').v2;


const client = new Client();

lang = 'en';

client.on('qr', (qr) => {
  // Generate and scan this code with your phone
  console.log('QR RECEIVED', qr);
});

client.on('ready', () => {
  console.log('Client is ready!');
});
const projectId = 'alltheforms-943f1';
const translate = new Translate({projectId});

async function translateText(text, target) {

  // Translates some text into Russian
  const [translation] = await translate.translate(text, target);
  console.log(`Text: ${text}`);
  console.log(`Translation: ${translation}`);
  return translation
}


client.on('message', async (msg) => {
  //language selection

  if ((msg.body == 'Hi') || (msg.body == 'Hey') || (msg.body == 'hi')) {
    lang = 'en';
    msg.reply(
      'Welcome to NitiEngine! Please provide more information about yourself:\n\n1. Age\n2. Gender\n3. Income\n4. State\n5. Occupation\n6. Number of children'
    );
  }
  else if (msg.body == 'नमस्ते') {
    lang = 'hi';
    msg.reply(
      'नितिइंजन में आपका स्वागत है! कृपया अपने बारे में अधिक जानकारी प्रदान करें:\n\n1. उम्र\n2. लिंग\n3. आय\n4. राज्य\n5. पेशा\n6. बच्चों की संख्या'
    );
  }
  else if (msg.body == 'नमस्ते') {
    lang = 'ma';
    msg.reply("PolicyEngine मध्ये आपले स्वागत आहे! कृपया तुमच्याबद्दल अधिक माहिती द्या:\n\n1. वय\n२. लिंग\n३. उत्पन्न\n४. राज्य\n५. व्यवसाय\n६. मुलांची संख्या"
    );
  }

  else if (msg.body == 'హలో') {
    lang = 'te';
    msg.reply(
      "PolicyEngineకి స్వాగతం! దయచేసి మీ గురించి మరింత సమాచారాన్ని అందించండి:\n\n1. వయస్సు\n2. లింగం\n3. ఆదాయం\n4. రాష్ట్రం\n5. వృత్తి\n6. పిల్లల సంఖ్య"
    );
  }

  else if (msg.body == 'வணக்கம்') {
    lang = 'ta';
    msg.reply(
      "PolicyEngine க்கு வரவேற்கிறோம்! உங்களைப் பற்றிய கூடுதல் தகவலை வழங்கவும்:\n\n1. வயது\n2. பாலினம்\n3. வருமானம்\n4. மாநிலம்\n5. தொழில்\n6. குழந்தைகளின் எண்ணிக்கை"
    );
  }

  else if (msg.body == 'નમસ્તે') {
    lang = 'ga';
    msg.reply(
      "PolicyEngine પર આપનું સ્વાગત છે! કૃપા કરીને તમારા વિશે વધુ માહિતી આપો:\n\n1. ઉંમર\n2. લિંગ\n3. આવક\n4. રાજ્ય\n5. વ્યવસાય\n6. બાળકોની સંખ્યા"
    );
  }
  else if (msg.body != 'dsfhfihafli') {
    if (lang != 'en') {
      // const { text } = await translate(response.data, { to: 'en' });
      // text = ""
      
      // translateText(msg.body, lang)
      //   .then(translatedText => {
      //     text =  translatedText ;
      //   })
      //   .catch(error => {
      //     console.error('Translation error:', error);
      //   });

      const queryParam = encodeURIComponent(msg.body);

      const url = `http://localhost:5000/query?query=${queryParam}`;

      axios.get(url)
        .then(response => {
          console.log('Response:', response.data);
          // text = await translate(response.data, { to: lang });
          // const { data } = await translate(response.data, { to: 'en' });
          // data = ""
          // translateText(response.data, lang)
          //   .then(translatedText => {
          //     data =  translatedText ;
          //   })
          //   .catch(error => {
          //     console.error('Translation error:', error);
          //   });
          msg.reply(response.data)
        })
        .catch(error => {
          console.error('Error:', error);
          msg.reply("Sorry, something went wrong. Please try again later.")
        });

    }
    else {
      const queryParam = encodeURIComponent(msg.body);

      const url = `http://localhost:5000/query?query=${queryParam}`;

      axios.get(url)
        .then(response => {
          console.log('Response:', response.data);
          msg.reply(response.data)
        })
        .catch(error => {
          console.error('Error:', error);
          msg.reply("Sorry, something went wrong. Please try again later.")
        });
    }
  }
});
client.initialize();
